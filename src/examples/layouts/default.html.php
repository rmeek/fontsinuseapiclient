<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php foreach ($stylesheets as $stylesheet): ?>
<link href="css/<?php echo $stylesheet ?>.css" rel="stylesheet" />
<?php endforeach ?>
</head>
<body>
<?php include $template ?>
</body>
</html>