FontsInUse API Client
==================

A client to access the FontsInUse.com API.
It handles the authentication protocol required for all requests.
Requires the cURL extension.

API Documentation can be found at `http://fontsinuse.com/doc/api/1`

Applications Ids and API keys can be requested at `http://fontsinuse.com/contact-us`

At the moment only a PHP client is available.

PHP5.3 is recommended.
PHP5.2 should also work.
For earlier versions of PHP you will need to include compatible versions of the json_encode and json_decode functions

## Usage ##


``` php
<?php

// PHP5.3+ (you will need to set up autoloading)

use Fb\FontsInUseAPI\Client;
$client = new Client($appId, $apiKey);
$client->setURLBase('http://fontsinuse.com/api/1/'); 
$data = $client->request('uses?family=helvetica&count=10');

// PHP5

include('/path/to/ ... /legacy/FontsInUseAPIClient.php');
$client = new FontsInUseAPIClient($appId, $apiKey);
$client->setURLBase('http://fontsinuse.com/api/1/'); 
$data = $client->request('uses?family=helvetica&count=10');

// PHP4

include('/path/to/ ... /legacy/FontsInUseAPIClient.php');
$client = new FontsInUseAPIClient();
$client->initialize($appId, $apiKey);
$client->setURLBase('http://fontsinuse.com/api/1/'); 
$data = $client->request('uses?family=helvetica&count=10');


```

## Examples ##

There is some PHP 5.3+ example code in src/examples

To try it out, rename the configure.php.def file to configure.php and fill out your app id and api key.

Now point your web server at index.php with the examples folder as your document root, and the examples should work.

Note that the examples are only a guide, and they are not intended for use as production code. The Client class is all you need to start integrating the Fonts in Use API into your app. 

