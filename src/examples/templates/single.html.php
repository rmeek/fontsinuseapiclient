<?php $use = $data['uses'][0]; ?>
<div class="fiu-use-wrapper">

<div class="fiu-use-head">
	<h1 class="fiu-use-title"><?php echo $use['name'] ?></h1>
	<div class="fiu-use-meta-top"></div>
</div><!-- fiu-use-head  -->

<div class="fiu-use-content"/>
<?php 
if ($use['source_type'] == 'Blog') {
	echo $use['text'];
} else {
	for ($i = 0; $i < count($use['media_items']); $i++) {
		$mediaItem = $use['media_items'][$i];?>
<div class="fiu-media-item">
<img src="<?php echo $mediaItem['src'] ?>" alt="<?php echo $mediaItem['filename'] ?>"/>
<div class="fiu-media-item-credits"><?php echo $mediaItem['credits'] ?></div>
<div class="fiu-media-item-caption"><?php echo $mediaItem['caption'] ?></div>
<?php if ($i == 0 && !empty($use['text'])):  ?>
<?php echo $use['text'] ?>
<?php endif  ?>
</div><!-- fiu-media-item -->
<?php 
	}
}
?>

<div class="fiu-meta">

<?php if ($use['font_families']): ?>
<h3>Fonts</h3>
<?php echo $helper->getSampleList($use['font_families']); ?>
<?php endif?>

<?php if ($use['formats']): ?>
<h3>Formats</h3>
<p><?php echo $helper->getTextList($use['formats'], array('useGalleryLinkType' => 'category', 'linkKey' => 'id')); ?></p>
<?php endif?>

<?php if ($use['industries']): ?>
<h3>Industries</h3>
<p><?php echo $helper->getTextList($use['industries'], array('useGalleryLinkType' => 'category', 'linkKey' => 'id')); ?></p>
<?php endif?>

<?php if ($use['designers']): ?>
<h3>Designers</h3>
<p><?php echo $helper->getTextList($use['designers'], array('useGalleryLinkType' => 'designer', 'linkKey' => 'id')); ?></p>
<?php endif?>

<?php if ($use['tags']): ?>
<h3>Tags</h3>
<p><?php echo $helper->getTextList($use['tags'], array('labelKey' => 'tag', 'useGalleryLinkType' => 'tag', 'linkKey' => 'id')); ?></p>
<?php endif?>
 
</div> <!-- fiu-meta -->

</div><!-- fiu-use-content -->


</div><!-- fiu-use-wrapper -->
