<?php

namespace Fb\FontsInUseAPI;

/**
 * Helper methods for views rendering with API data
 */
class ViewHelper {
	
	public $baseBrowseLink;
	
	/**
	 * Get the thumbail img element for a use
	 * @param array $use
	 * @param array $options 
	 * @return string the image element
	 */
	public function getThumb($use, $options = array()) {
		$options = array_merge(array(
			'class' => 'fiu-thumb',
		), $options);
		return '<img class="' . $options['class'] . '" src="' . $use['thumb'] . '" alt="' . $use['name'] . '"/>';
	}
	
	/**
	 * Get the opening element of an <a> link for a use
	 * @param array $use
	 * @param array $options 
	 * @return string the a element
	 */
	public function getOpenUseLink($use, $options = array()) {
		$options = array_merge(array(
			'target' => '_blank',
			'class' => 'fiu-use-link'
		), $options);		
		return '<a class="' . $options['class']. '" target="' . $options['target'] . 
			'" href="' . $use['permalink'] . '" title="' . $use['name'] . '">';
	}
	
	/**
	 * Get a sample img element for a family
	 * @param array $family
	 * @return string the image element
	 */
	public function getFamilySample($family, $options = array()) {
		return '<img src="' .  $family['sample_src'] . '" alt="' . $family['name'] . '"/>';
	}
	
	/**
	 * Get a font family link
	 * @param array $family
	 * @param array $options 
	 * @return string
	 */
	public function getFamilyLink($family, $options = array()) {
		$options = array_merge(array(
			'target' => '_blank',
			'label' => $family['name']
		), $options);
		return '<a target="' . $options['target'] . '" href="' .  $family['permalink'] . 
			'" title="' .  $family['name'] . '">' . $options['label'] . '</a>';
	}
	
	/**
	 * Get a list of linked family samples
	 * @param array $families
	 * @param array $options 
	 * @return string
	 */
	public function getSampleList($families, $options = array()) {
		$options = array_merge(array(
			'limit' => 0,
		), $options);
		$res = '';
		if (!empty($families)) {
			$res .= '<ul class="fiu-sample-list">';
			$count = 0;
			foreach ($families as $family) {
				if (!empty($family['sample_src'])) {
					$count++;
					$res .= '<li>';
					$res .= $this->getFamilyLink($family, array(
							'label' => $this->getFamilySample($family), 
							'target' => 'fontsinuse'
						)); 
					$res .= '</li>';
				}
				if ($options['limit'] && $count == $options['limit']) {
					break;
				}
			}
			$res .= '</ul>';
		}
		return $res;
	}
	
	/**
	 * Get a list of items in a free, textual form
	 * @param array $items
	 * @param array $options
	 */
	public function getTextList($items, $options = array()) {
		$options = array_merge(array(
			'labelKey' => 'name',
			'linkKey' => 'permalink',
			'separator' => ', ',
			'finalSeparator' => ' and ',
			'target' => '_blank',
			'useGalleryLinkType' => null
		), $options);
		$total = count($items);
		$res = '';
		for ($i = 0; $i < $total; $i++) {
			$item = $items[$i];
			$href = $item[$options['linkKey']];
			if ($options['useGalleryLinkType']) {
				$href = $this->getBrowseLink('uses?' . $options['useGalleryLinkType'] . '=' . $href);
			}
			$res .= '<a target="' . $options['target'] . '" href="' . 
				$href . '">' . $item[$options['labelKey']] . '</a>';
			if ($i == $total - 2) {
				$res .=  $options['finalSeparator'];
			} else if ($i !== ($total - 1)) {
				$res .=  $options['separator'];
			}
		}
		return $res;
	}
	
	public function setBrowseLink($url) {
		$this->baseBrowseLink = $url;
	}
	
	public function getBrowseLink($url) {
		return $this->baseBrowseLink . '&url=' . urlencode($url);
	}
	
	/**
	 * Get a simple (more/back) pagination element
	 * @param array $data
	 * @param array $options
	 * @return string
	 */
	public function getPagination($data, $options = array()) {
		$options = array_merge(array(
			'moreLabel' => '&gt;&gt;',
			'backLabel' => '&lt;&lt;',
		), $options);
		if (!isset($data['count'])) {
			return;
		}
		$totalItems = $data['count'];
		$page = $data['page'];
		$pageSize = $data['page_size'];
		$maxPage = ceil($totalItems / $pageSize);
		$res = '';
		if ($page > 1) {
			$link = $this->setUrlParam($data['url'], $page - 1);
			$res .= '<span class="fiu-gallery-back"><a href="' . $link . '">' . $options['backLabel'] . '</a></span>';
		} 		
		if ($page < $maxPage) {
			$link = $this->setUrlParam($data['url'], $page + 1);
			$res .= '<span class="fiu-gallery-more"><a href="' . $link . '">' . $options['moreLabel'] . '</a></span>';
		}
		return $res;
	}
	
	private function setUrlParam($url, $value, $key = 'page') {
		$newParam = $key . '=' . $value;
		$url = preg_replace('/' . $key . '=(\d+)/i', $newParam, $url, -1, $replaced);
		if (!$replaced) {
			$url = $url . '&' . $newParam;
		}
		$request = preg_replace('/url=[^&]+/i', 'url=' . urlencode($url), $_SERVER['REQUEST_URI']);
		return $request;
	}
	
	

	
	
}