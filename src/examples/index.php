<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/examples.css" rel="stylesheet" />
</head>
<body >

<?php 

	$examples = array(
		'fiu-gallery' => array(
			'description' => 'Gallery like FontsInUse.com with links going to FontsInUse.com.',
			'template' => 'fiu-gallery.html.php',
			'url' => 'uses?count=16&sort=published&order=desc',
			'css' => 'common|fiu-gallery'
		),
		'gallery' => array(
			'description' => 'Simple gallery integration with links going to single page views',
			'template' => 'simple-gallery.html.php',
			'url' => 'uses?count=16',
			'css' => 'common|simple-gallery'
		),
	);
	$keys = array_keys($examples);
	$id = isset($_GET['example'])? $_GET['example'] : $keys[0];
?>
<form>
<select name="example" onchange="javascript:forms[0].submit()">
<?php foreach ($examples as $key => $example): ?>
<option value="<?php echo $key  ?>" <?php if ($id == $key): ?>selected<?php endif?>><?php echo $example['description']?></option>
<?php endforeach ?>
</select>
</form>

<iframe id="fiu" src="/render.php?template=<?php echo $examples[$id]['template'] ?>&stylesheet=<?php echo $examples[$id]['css'] ?>&url=<?php echo urlencode($examples[$id]['url']) ?>"></iframe>

</body>
</html>