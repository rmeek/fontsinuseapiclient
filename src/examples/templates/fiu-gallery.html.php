<!-- FontsInUse.com gallery start  -->
<div class="fiu-gallery">

<ul class="fiu-gallery-list">

<?php foreach ($data['uses'] as $use): ?>
<li class="fiu-gallery-use">
<?php echo $helper->getOpenUseLink($use, array('target' => 'fontsinuse')); ?>
<div class="fiu-thumb-wrapper">
<?php if ($use['thumb']): ?>
<?php echo $helper->getThumb($use); ?>
<?php endif ?>
</div>
<div class="fiu-gallery-use-teaser">
<h3><?php echo $use['name'] ?></h3>
</div>
</a>
<?php echo $helper->getSampleList($use['font_families'], array('limit' => 4)) ?>
</li>
<?php endforeach ?>

</ul>
<br class="clear"/>
<div class="fiu-pagination">
<?php echo $helper->getPagination($data, array('backLabel' => 'Back', 'moreLabel' => 'More…')); ?>
</div>

</div>
<!-- FontsInUse.com gallery end  -->
