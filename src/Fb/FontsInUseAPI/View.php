<?php
namespace Fb\FontsInUseAPI;

/**
 * Super simple View class used by the examples
 */
class View {

	public function render($template, $data, $options = array()) {
		$options = array_merge(array(
			'layout' => null,
			'stylesheets' => array(),
			'baseBrowseLink' => null,
		), $options);
		$stylesheets = $options['stylesheets'];
		$helper = new ViewHelper();
		$helper->baseBrowseLink = $options['baseBrowseLink'];
		if ($options['layout']) {
			include($options['layout']);
		} else {
			include($template);
		}
	}
	
}
