<?php

/**
* An OPTIONAL autoloader to load client classes
* automatically – or use/roll your own.
*/

if (defined('FIUAPICLIENT_AUTOLOADER')) {
	return;
}

define('FIUAPICLIENT_AUTOLOADER', true);


function FontsInUseAPIClientAutoload($class) {
	
	$path = dirname(__FILE__) . '/' . str_replace("\\", "/", $class) . '.php';
	if (file_exists($path)) {
		require_once($path);
	} else {
		die('Class ' . $class . ' not found at ' . $path);
	}
}

spl_autoload_register('FontsInUseAPIClientAutoload');