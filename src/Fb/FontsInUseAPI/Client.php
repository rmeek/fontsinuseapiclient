<?php

namespace Fb\FontsInUseAPI;

/**
 * Client for the FontsInUse.com API
 * requires cURL
 * Construct an instance with a valid appId and key, then call the request method to get data
 * API doc at http://fontsinuse.com/doc/api/1
 */
class Client {
	
	protected $appId;
	protected $key;
	protected $handle;
	protected $logger;
	protected $debug;
	protected $URLBase;
	
	/**
	 * Construct a new Client
	 * @param string $appId Application identifier supplied by FontsInUse.com
	 * @param string $key Secret application key supplied by FontsInUse.com
	 */
	public function __construct($appId, $key) {
		$this->appId = $appId;
		$this->key = $key;
		$this->URLBase = '';
	}

	
	/**
	 * Make an API request
	 * @param string $url
	 * $param boolean $decode whether to return decoded JSON as an associative array or leave it as a string
	 * @return mixed api response as array or JSON string
	 */
	public function request($url, $decode = true) {
		$url = $this->URLBase . $url;
		$nonce = hash('sha512', $this->makeRandomString());
		$created = date(DATE_ATOM);
		$digest = base64_encode(sha1(base64_decode($nonce) . $created . $this->key, true));
		$curl = $this->getHandle();
		curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_HTTPHEADER => array(
						'Authorization: WSSE profile="UsernameToken"',
						'X-WSSE: UsernameToken Username="' . $this->appId . '",
						PasswordDigest="' . $digest . '",
						Nonce="' . $nonce . '",
						Created="' . $created . '"'
				),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HEADER => true,
		));
		$res = curl_exec($curl);
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		if ($status != 200) {
			$this->log($res);
			$data = array('status' => $status);
			if ($err = curl_errno($curl)) {
				$this->log('error: ' . curl_error($curl));
				$data['error'] = curl_error($curl);
			}
		} else {
			list($headers, $body) = $this->parseResponse($curl, $res);
			$data = json_decode($body, true);
			if (isset($headers['fontsinuseapi-request-quota'])) {
				$data['request_quota'] = $headers['fontsinuseapi-request-quota'];
			}
			if (isset($headers['fontsinuseapi-cached-data'])) {
				$data['cached_response'] = $headers['fontsinuseapi-cached-data'];
			}
			$data['url'] = $url;
		}
		if (!$decode) {
			$data = json_encode($data);
		}
		return $data;
	}
	
	/**
	 * Set a simple logging class to activate logging for the client
	 * The logging class has to have a public method with the signature log($message)
	 * @param unknown_type $logger
	 */
	public function setLogger($logger) {
		$this->logger = $logger;
	}
	
	/**
	 * Sets a convenience prefix for all URLs. Saves entering domain names and the base path for the API
	 * when using the request method.
	 * @example $client->setURLBase('http://fontsinuse.com/api/1/');
	 * @param string $base
	 */
	public function setURLBase($base) {
		$this->URLBase = $base;
	}
	
	private function getHandle() {
		return $this->handle? $this->handle : curl_init();
	}
	
	private function log($s) {
		if ($this->logger) {
			$this->logger->log($s);
		}
	}
	
	/**
	 * Split the curl response into body and a headers array
	 * @param $curl the curl handle which provided the response
	 * @param $res the full response from curl_exec
	 * @return array two entries, response headers array first, then the response body
	 */
	private function parseResponse($curl, $res) {
		$headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = substr($res, 0, $headerSize);
		$lines = explode("\n", $header);
		$headers = array();
		foreach ($lines as $line) {
			$line = trim($line);
			$parts = explode(':', $line, 2);
			if (count($parts) > 1) {
				$headers[trim($parts[0])] = trim($parts[1]);
			} elseif (!empty($line)) {
				$headers[] = $line;
			}
		}
		$body = substr($res, $headerSize);
		return array($headers, $body);
	}
	
	/**
	 * Make a random string
	 * @param integer $bits
	 * @return string
	 */
	private function makeRandomString($bits = 256) {
		$bytes = ceil($bits / 8);
		$rand = '';
		for ($i = 0; $i < $bytes; $i++) {
			$rand .= chr(mt_rand(0, 255));
		}
		return $rand;
	}
	
	/**
	 * Destructor
	 */
	public function __destruct() {
		if ($this->handle) {
			curl_close($this->handle);
		}
	}
		
}