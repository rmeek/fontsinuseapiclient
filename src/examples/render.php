<?php 

require_once('configure.php');

require_once('../autoload.php');
use Fb\FontsInUseAPI\Client;
use Fb\FontsInUseAPI\View;

// get request parameters
$url = urldecode($_GET['url']);
$template = 'templates/' . $_GET['template'];
$css = explode('|', $_GET['stylesheet']);

// set up the client
$client = new Client($fiuApiId, $fiuApiKey);
$client->setURLBase($fiuBase);

// make the request
$data = $client->request($url);

// create and render the view
$view = new View();
$view->render(
	$template, 
	$data,
	array(
		'stylesheets' => $css,
		'layout' => 'layouts/default.html.php',
		'baseBrowseLink' => 'render.php?template=simple-gallery.html.php&stylesheet=common|simple-gallery'
	)
);

