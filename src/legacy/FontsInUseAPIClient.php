<?php

/**
 * php4/php5.2- compatible client for the FontsInUse.com API
 * requires cURL
 * Construct an instance with a valid appId and key, then call the request method to get data
 * API doc at http://fontsinuse.com/doc/api/1
 */
class FontsInUseAPIClient {

	var $__appId;
	var $__key;
	var $__handle;
	var $__logger;
	var $__debug;
	var $__URLBase;
	
	/**
	 * php5 constructor
	 * @param string $appId Application identifier supplied by FontsInUse.com
	 * @param string $key Secret application key supplied by FontsInUse.com
	 */
	function __construct($appId, $key) {
		$this->initialize($appId, $key);
	}
	
	/**
	 * Only needs to be called after construction in php4
	 * Initializes the client with security credentials
	 * @param string $appId
	 * @param string $key
	 */
	function initialize($appId, $key) {
		if (!function_exists('json_decode')) {
			die('JSON encode/decode is required by ' . __CLASS__ . '. This means you either ' . 
				'need to upgrade to PHP5.2 or you need to find a PHP4 compatible ' . 
				'implementation of json_encode and json_decode');
		}
		if (!function_exists('curl_init')) {
			die('cURL is required by ' . __CLASS__ . '.');
		}	
		register_shutdown_function(array(&$this, '__destruct'));
		$this->__appId = $appId;
		$this->__key = $key;	
		$this->__URLBase = '';	
	}
	
	/**
	 * Make an API request
	 * @param string $url
	 * $param boolean $decode whether to decode JSON as an associative array or leave it as a string
	 * @return string api response as JSON
	 */
	function request($url, $decode = true) {
		$url = $this->__URLBase . $url;
		$nonce = hash('sha512', $this->__makeRandomString());
		$created = date(DATE_ATOM);
		$digest = base64_encode(sha1(base64_decode($nonce) . $created . $this->__key, true));
		$curl = $this->__getHandle();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array(
				'Authorization: WSSE profile="UsernameToken"',
				'X-WSSE: UsernameToken Username="' . $this->__appId . '",
				PasswordDigest="' . $digest . '",
				Nonce="' . $nonce . '",
				Created="' . $created . '"'
			),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true,
		));
		$res = curl_exec($curl);
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		if ($status != 200) {
			$this->__log($res);
			$data = array('status' => $status);
			if ($err = curl_errno($curl)) {
				$this->__log('error: ' . curl_error($curl));
				$data['error'] = curl_error($curl);
			}			
		} else {
			list($headers, $body) = $this->__parseResponse($curl, $res);
			$data = json_decode($body, true);
			if (isset($headers['fontsinuseapi-request-quota'])) {
				$data['request_quota'] = $headers['fontsinuseapi-request-quota'];
			}
			if (isset($headers['fontsinuseapi-cached-data'])) {
				$data['cached_response'] = $headers['fontsinuseapi-cached-data'];
			}
			$data['url'] = $url;
		}
		if (!$decode) {
			$data = json_encode($data);
		}
		return $data;
	}
	
	/**
	 * Set a simple logging class to activate logging for the client
	 * The logging class has to have a public method with the signature log($message)
	 * @param unknown_type $logger
	 */
	function setLogger($logger) {
		$this->__logger = $logger;
	}
	
	/**
	 * Sets a convenience prefix for all URLs. Saves entering domain names and the base path for the API 
	 * when using the request method.
	 * @example $client->setURLBase('http://fontsinuse.com/api/1/'); 
	 * @param string $base
	 */
	function setURLBase($base) {
		$this->__URLBase = $base;
	}
	
	function __getHandle() {
		return $this->__handle? $this->__handle : curl_init();
	}
	
	function __log($s) {
		if ($this->__logger) {
			$this->__logger->log($s);
		}
	}
	
	/** 
	 * Split the curl response into body and a headers array
	 * @param $curl the curl handle which provided the response
	 * @param $res the full response from curl_exec
	 * @return array two entries, response headers array first, then the response body
	 */
	function __parseResponse($curl, $res) {
		$headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$header = substr($res, 0, $headerSize);
		$lines = explode("\n", $header);
		$headers = array();
		foreach ($lines as $line) {
			$line = trim($line);
			$parts = explode(':', $line, 2);
			if (count($parts) > 1) {
				$headers[trim($parts[0])] = trim($parts[1]);
			} elseif (!empty($line)) {
				$headers[] = $line;
			}
		}
		$body = substr($res, $headerSize);	
		return array($headers, $body);
	}

	/**
	 * Make a random string
	 * @param integer $bits
	 * @return string
	 */
	function __makeRandomString($bits = 256) {
	    $bytes = ceil($bits / 8);
	    $rand = '';
	    for ($i = 0; $i < $bytes; $i++) {
	        $rand .= chr(mt_rand(0, 255));
	    }
		return $rand;
	}
	
	/**
	 * Destructor
	 */
	function __destruct() {
		if ($this->__handle) {
			curl_close($this->__handle);
		}
	}
	
}