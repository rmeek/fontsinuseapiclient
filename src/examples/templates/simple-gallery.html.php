<!-- FontsInUse.com gallery start  -->
<div class="fiu-gallery">

<ul class="fiu-gallery-list">
<?php foreach ($data['uses'] as $use): ?>
<li class="fiu-gallery-use">
<a href="/render.php?template=single.html.php&stylesheet=common|single&url=<?php echo urlencode('uses/' . $use['id']) ?>">
<div class="fiu-thumb-wrapper">
<?php if ($use['thumb']): ?>
<img class="fiu-gallery-thumb" src="<?php echo $use['thumb'] ?>" alt="<?php echo $use['name'] ?>"/>
<?php endif ?>
</div>
</a>
<h3><?php echo $use['name'] ?></h3>
<p class="fiu-gallery-families">Using <?php 
	echo $helper->getTextList($use['font_families'], array('useGalleryLinkType' => 'family', 'linkKey' => 'id'));
?></p>
</li>
<?php endforeach ?>

</ul>
<br class="clear"/>
<div class="fiu-pagination">
<?php echo $helper->getPagination($data, array('backLabel' => 'Back', 'moreLabel' => 'More…')); ?>
</div>
<!-- FontsInUse.com gallery end  -->
